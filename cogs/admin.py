""" Admin commands """
import discord
from discord.ext import commands


class Admin(commands.Cog):    
    def __init__(self, bot):
        bot.remove_command('help')
        self.bot = bot

    @commands.command(aliases=["?"])
    async def help(self, ctx):
        """Sends help message """
        await ctx.send("https://discordapp.com/oauth2/authorize?client_id=649351968623427640&scope=bot&permissions=1110453312")

    @commands.command(alias=["scram", "leave", "dc"])
    async def disconnect(self, ctx):
        # Find the voice client for this server
        self.vc = discord.utils.get(self.bot.voice_clients, guild=ctx.guild)
        if self.vc:
            self.vc.disconnect(force=True)


def setup(bot):
    bot.add_cog(Admin(bot))
