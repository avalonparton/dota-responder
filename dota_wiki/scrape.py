import requests
from bs4 import BeautifulSoup
from time import sleep
import json
import unicodedata


HEROES = ["Abaddon", "Alchemist", "Ancient Apparition", "Anti-Mage", "Arc Warden", "Axe", "Bane", "Batrider", "Beastmaster", "Bloodseeker", "Bounty Hunter", "Brewmaster", "Bristleback", "Broodmother", "Centaur Warrunner", "Chaos Knight", "Chen", "Clinkz", "Clockwerk", "Crystal Maiden", "Dark Seer", "Dark Willow", "Dazzle", "Death Prophet", "Disruptor", "Doom", "Dragon Knight", "Drow Ranger", "Earth Spirit", "Earthshaker", "Elder Titan", "Ember Spirit", "Enchantress", "Enigma", "Faceless Void", "Grimstroke", "Gyrocopter", "Huskar", "Invoker", "Io", "Jakiro", "Juggernaut", "Keeper of the Light", "Kunkka", "Legion Commander", "Leshrac", "Lich", "Lifestealer", "Lina", "Lion", "Lone Druid", "Luna", "Lycan", "Magnus", "Mars", "Medusa", "Meepo", "Mirana", "Monkey King", "Morphling", "Naga Siren", "Nature's Prophet", "Necrophos", "Night Stalker", "Nyx Assassin", "Ogre Magi", "Omniknight", "Oracle", "Outworld Devourer", "Pangolier", "Phantom Assassin", "Phantom Lancer", "Phoenix", "Puck", "Pudge", "Pugna", "Queen of Pain", "Razor", "Riki", "Rubick", "Sand King", "Shadow Demon", "Shadow Fiend", "Shadow Shaman", "Silencer", "Skywrath Mage", "Slardar", "Slark", "Snapfire", "Sniper", "Spectre", "Spirit Breaker", "Storm Spirit", "Sven", "Techies", "Templar Assassin", "Terrorblade", "Tidehunter", "Timbersaw", "Tinker", "Tiny", "Treant Protector", "Troll Warlord", "Tusk", "Underlord", "Undying", "Ursa", "Vengeful Spirit", "Venomancer", "Viper", "Visage", "Void Spirit", "Warlock", "Weaver", "Windranger", "Winter Wyvern", "Witch Doctor", "Wraith King", "Zeus"]

def get_thumbnail(response):
    """ hard coding the thumbnails here """
    name = response['name']

    # Default
    url = "https://icon-library.net/images/dota-2-icon/dota-2-icon-28.jpg"

    # Voice packs - get the text inside parens
    if "(" in name:
        name = name.split('(')[1].split(')')[0]

    # Other Responses    
    if name == "Warlock's Golem":
        url = "https://i.imgur.com/F49q2Gf.png"
    elif name == "Shopkeeper":
        url = "https://i.imgur.com/Xyf1VjQ.png"
    elif name == "Announcer":
        url = "https://i.imgur.com/B8pD7m2.png"
    
    # Announcer Packs
    elif name == "Gabe Newell":
        url = "https://i.imgur.com/a1tnZusr/bin/python3.8 -u /opt/dotabot/dotabot.pyAw.png"

    # Heroes
    elif name == "Skeleton King":
        url = "https://i.imgur.com/WEwzR0Z.png"
    elif name in HEROES:
        hero = name.replace(' ', '_').replace('-', '').lower()
        url = f"https://api.opendota.com/apps/dota2/images/heroes/{hero}_full.png"

    return url


def scrape_responses(url) -> list:
    """ Returns a list of dicts of the form:
    {'url': "https://..", 'text': "Axe Attacks!", 'thumbnail': "https://.."}
    """
    responses = []
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    content = soup.find(id='mw-content-text')

    # Remove non-audio links
    for elem in soup.find_all(class_='notanavbox') + soup.find_all(id='toc'):
        elem.decompose()

    for li in content.find_all('li'):
        response =  dict()
        # Find the audio source link
        link = li.find('a')
        if link:
            response['url'] = link.get('href')

            # Remove unused elements
            for elem in li.find_all('span') + li.find_all(class_='tooltip'):
                elem.decompose()

            # Replace elipses with periods
            response['text'] = li.text.strip().replace("…", "...")

            # Only add response if there's a valid URL (some responses still missing links)
            if response['url'] and "http" in response['url'] and response['text'] != "":
                responses.append(response)

    return responses

def get_response_urls() -> list:
    """ Returns a list of URLs and their names for all Response pages from: """
    url = "https://dota2.gamepedia.com/Template:VoiceNavSidebar"
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    content = soup.find(id='mw-content-text')

    # Remove non-response links in header and footer of table
    content.find(class_='notanavbox-title').decompose()
    content.find(class_='notanavbox-abovebelow').decompose()

    # Find all URLs
    urls = []
    for elem in content.find_all('a'):
        url = f"https://dota2.gamepedia.com{elem['href']}"
        # Replace space with space
        name = elem.text.replace('\xa0',' ')
        urls.append((name, url))
    
    return urls

def main():
    responses = []
    # Get every response page from the table
    response_pages = get_response_urls()
    for name, response_url in response_pages:
        print(f"Scraping responses for {name} from {response_url}")

        for response in scrape_responses(response_url):
            
            r = dict(name=name,
                     responses=response_url,
                     url=response['url'],
                     text=response['text'])
            r['thumbnail'] = get_thumbnail(r)
            #print(" --> ", r)
            responses.append(r)

    with open('responses.json', 'w+') as fp:
        json.dump(responses, fp, indent=4)


main()
