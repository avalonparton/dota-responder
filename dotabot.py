from random import randint

import logging
import discord
from discord.ext import commands
from keys import discord_token

# Disables Discord logs (they're huge)
logging.getLogger('discord').disabled = True 

def get_prefix(_):
    return "dota"

class DotaBot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=get_prefix, case_insensitive=True)
        
        # Load cogs
        self.load_extension('cogs.admin')
        self.load_extension('cogs.error_handler')
        self.load_extension('cogs.dota')

        @self.event
        async def on_ready():
            """ Called after the bot successfully connects to Discord servers """
            print(f"Connected as {self.user.display_name}")

            # Change presence to "Playing dota sounds in 69 guilds"
            text = f"dota sounds in {len(self.guilds)} guilds"
            activity = discord.Activity(name=text, type=discord.ActivityType.streaming)
            await self.change_presence(activity=activity) 

            # Print guild info
            print(f"Active in {len(self.guilds)} guilds:")
            user_count = 0
            for guild in self.guilds:
                try:
                    count = len(guild.members) - 1 # remove self from list
                    print(f" - [{guild.id}] ({count} users) {guild.name} (Owner: {guild.owner.name}#{guild.owner.discriminator} {guild.owner.id})")
                    # add user count, exclude discord bot list
                    if guild.id != 264445053596991498:
                        user_count += count
                except AttributeError:
                    pass
            print(f"Total user reach: {user_count}")

        @self.event
        async def on_guild_join(guild):
            channel = discord.utils.get(guild.text_channels, name="general")
            if len(guild.text_channels) > 0 and channel is None:
                channel = guild.text_channels[0]
            await channel.send('Hello! You can send either a full quote with exact punctuation ("Haha!") or a partial quote prefixed by "dota" ("dota haha")')
            await channel.send('Support server: https://discord.gg/Czj2g9c')

    async def send_embed(self, channel, color=None, footer=None, footer_icon=None, subtitle=None,
        subtext=None, text=None, title=None, thumbnail=None):
        """ Sends a message to a channel, and returns the discord.Message of the sent message.

        If the text is over 2048 characters, subtitle and subtext fields are ignored and the
        message is split up into chunks. The first message will have the title and thumbnail,
        and only the last message will have the footer. 
        
        Returns a list of messages sent.
        """
        MSG_LIMIT = 2048
        messages = []

        # Use a random color if none was given
        if color is None:
            color = randint(0, 0xFFFFFF)

        # Text fits into one message, add all fields passed to function
        if text is None or len(text) <= MSG_LIMIT:
            embed = discord.Embed(color=color)
            if footer is not None:
                if footer_icon is not None:
                    embed.set_footer(text=footer, icon_url=footer_icon)
                else:
                    embed.set_footer(text=footer)
            if subtitle is not None or subtext is not None:
                embed.add_field(name=subtitle, value=subtext, inline=True)
            if thumbnail is not None:
                embed.set_thumbnail(url=thumbnail)
            if title is not None:
                embed.title = title
            if text is not None:
                embed.description = text
            
            # Send the single message
            messages.append(await channel.send(embed=embed))

        # Text must be broken up into chunks
        else:
            i = 0 # message index
            lines = text.split("\n")
            while lines:
                # Construct the text of this message
                text = ""

                while lines and len(text + lines[0]) < MSG_LIMIT:
                    text = text + "\n" + lines.pop(0)

                embed = discord.Embed(color=color)
                embed.description = text

                # First message in chain - add the title and thumbnail
                if i == 0:
                    if title is not None:
                        embed.title = title
                    if thumbnail is not None:
                        embed.set_thumbnail(url=thumbnail)
                    if subtitle is not None or subtext is not None:
                        embed.add_field(name=subtitle, value=subtext, inline=True)

                # Last message in chain - add the footer
                if not lines:
                    if footer is not None:
                        if footer_icon is not None:
                            embed.set_footer(text=footer, icon_url=footer_icon)
                        else:
                            embed.set_footer(text=footer)

                messages.append(await channel.send(embed=embed))
                i = i + 1

        # Return the list of messages sent so reactions can be easily added
        return messages

    async def add_reactions(self, message, emojis):
        """ Adds a list of reactions to a message, ignoring NotFound errors """
        try:
            for emoji in emojis:
                await message.add_reaction(emoji)
        except discord.errors.NotFound:
            pass
    
    async def delete_message(self, message):
        """ Deletes a message, ignoring NotFound errors """
        if message is not None:
            try:
                await message.delete()
            except discord.errors.NotFound:
                pass

def main():
    bot = DotaBot()
    @bot.event
    async def on_voice_state_update(member, before, after):
        
        # Intro voice line!
        # plomdawg
        if member.id == 163040232701296641 and before.channel is None and after.channel is not None:
            # Avec Aplomb!
            url = "https://static.wikia.nocookie.net/dota2_gamepedia/images/4/4f/Vo_pangolin_pangolin_move_09.mp3/revision/latest?cb=20200524131747"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # justin
        elif member.id == 129736618322952193 and before.channel is None and after.channel is not None:
            # That's the biggest banana slug I've ever seen!
            url = "https://static.wikia.nocookie.net/dota2_gamepedia/images/e/e4/Vo_monkey_king_monkey_rival_202.mp3/revision/latest?cb=20201017125437"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # kaiser
        elif member.id == 174246888592113665 and before.channel is None and after.channel is not None:
            # Masterful Mark!
            url = "https://static.wikia.nocookie.net/dota2_gamepedia/images/c/c1/Vo_announcer_dlc_ti10_vog_vog_ti_ann_scribblin_03_02.mp3/revision/latest?cb=20200821214324"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # kiwi
        elif member.id == 135967231157862401 and before.channel is None and after.channel is not None:
            url = "https://hickneller.com/media/kiwi.mp3"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # waqas
        elif member.id == 174251292917628929 and before.channel is None and after.channel is not None:
            # Know any weeds need whackin'? That's your fella.
            url = "https://static.wikia.nocookie.net/dota2_gamepedia/images/b/be/Vo_announcer_dlc_bastion_announcer_pick_goblinshredder_follow_03.mp3/revision/latest?cb=20201006202420"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # rachel
        elif member.id == 691825200815276112 and before.channel is None and after.channel is not None:
            # That's the biggest banana slug I've ever seen!
            url = "https://static.wikia.nocookie.net/dota2_gamepedia/images/e/e4/Vo_monkey_king_monkey_rival_202.mp3/revision/latest?cb=20201017125437"
            await bot.get_cog('Dota').play_response(after.channel, url)

        # Leave if nobody is in the channel with the bot.
        vc = discord.utils.get(bot.voice_clients, guild=member.guild)
        if vc and vc.channel:
            if not any([not user.bot for user in vc.channel.members]):
                await vc.disconnect()

    bot.run(discord_token)

if __name__ == '__main__':
    main()
