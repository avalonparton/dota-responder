#!/bin/bash
# deploy.sh - installs a service to run the bot

function echo_blue(){ echo -e "\n\033[1;34m$@\033[0m\n"; }

# Install apt package
sudo add-apt-repository --yes ppa:deadsnakes/ppa
echo_blue "apt update -y"
sudo apt update -y
echo_blue "sudo apt install -y python3.8 python3-pip libffi-dev libnacl-dev"
sudo apt install -y python3.8 python3-pip libffi-dev libnacl-dev

# Install pip packages
sudo python3.8 -m pip install --upgrade -r ${CI_PROJECT_DIR}/requirements.txt

# Install snap packages
echo_blue "sudo snap install ffmpeg"
sudo snap install ffmpeg

# Install dotabot
echo_blue "----- INSTALLING DOTABOT -----"

WorkingDirectory=/opt/dotabot
InstallDirectory=/opt/dotabot
sudo mkdir -p ${WorkingDirectory} ${InstallDirectory}

# Clean install dir
echo_blue "Cleaning install directory:"
sudo rm -rfv ${InstallDirectory}/cogs
sudo rm -rfv ${InstallDirectory}/dota_wiki
sudo rm -rfv ${InstallDirectory}/dotabot.py
sudo rm -rfv ${InstallDirectory}/responses.sql*

# Move to install dir
echo_blue "Moving to install directory:"
sudo mv -v ${CI_PROJECT_DIR}/cogs ${InstallDirectory}
sudo mv -v ${CI_PROJECT_DIR}/dota_wiki ${InstallDirectory}
sudo mv -v ${CI_PROJECT_DIR}/dotabot.py ${InstallDirectory}

# Decode secret key file.
echo "${KEYFILE}" | base64 -d | sudo tee "${InstallDirectory}/keys.py" > /dev/null

### /etc/systemd/system/dotabot.service
echo_blue "Creating /etc/systemd/system/dotabot.service:"
cat << EOF | sudo tee /etc/systemd/system/dotabot.service
[Unit]
Description=dotabot
After=multi-user.target
[Service]
User=root
Group=root
Type=idle
WorkingDirectory=${WorkingDirectory}
ExecStart=/usr/bin/python3.8 -u ${InstallDirectory}/dotabot.py
Restart=always
RestartSec=1
[Install]
WantedBy=multi-user.target
EOF

#### Reload systemctl to trigger new service
sudo systemctl daemon-reload
sudo systemctl stop dotabot || true
sudo systemctl start dotabot.service
sudo systemctl enable dotabot.service
sleep 1

echo_blue "systemctl status"
systemctl status dotabot

echo_blue "----- DOTABOT INSTALLED SUCCESSFULLY -----"
